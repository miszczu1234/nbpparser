/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import org.apache.log4j.Logger;
import pl.parser.nbp.exception.InputDataNotValid;
import pl.parser.nbp.program.InputDataParser;
import pl.parser.nbp.program.ProgramAlgorithm;

/**
 *
 * @author kurekk
 */
public class MainClass {
    
    private static final Logger LOG= Logger.getLogger(MainClass.class);
    
    public static void main(String args[]){
        try{
            LOG.info(String.format("Argumenty wejsciowe programu : %s", Arrays.toString(args)));
            InputDataParser inputDataParser= new InputDataParser(args);
            inputDataParser.parseInputData();
            ProgramAlgorithm program= new ProgramAlgorithm(inputDataParser.getBeginDate(), 
                    inputDataParser.getEndDate(), 
                    inputDataParser.getCurrencyCode());
            program.process();
            float standardDev= program.getStandardDeviation();
            float avgRateSelling= program.getAvgRateSelling();
            LOG.info(String.format("Sredni kurs kupna : %f", avgRateSelling));
            LOG.info(String.format("Odchylenie standardowe kursow sprzedazy : %f", standardDev));
        }catch(InputDataNotValid e){
            LOG.info(e.getMessage());
        }catch(ParseException e){
            LOG.info(String.format("Niepoprawny format daty : %s", e));
        }catch(IOException e){
            LOG.info(String.format("Błąd wejścia wyjścia : %s", e));
        }
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.math;

import java.util.List;
import pl.parser.nbp.data.model.Position;

/**
 *
 * @author kurekk
 */
public class Math {
    public float calculateAvgRateBuying(List<Position> positions){
        
        float avg = 0.0f;
        float sum= 0.0f;
        
        for(Position position : positions){
            sum+=position.getRateBuying();
        }
        
        avg= sum/positions.size();
        
        return avg;
    }
    
    public float calculateAvgRateSelling(List<Position> positions){
        
        float avg = 0.0f;
        float sum= 0.0f;
        
        for(Position position : positions){
            sum+=position.getRateSelling();
        }
        
        avg= sum/positions.size();
        
        return avg;
    }
    
    public float calculateStandardDeviation(List<Position> positions){
        
        float avg= calculateAvgRateSelling(positions);
        float result= 0;
        
        for(Position position : positions){
            float rateSelling= position.getRateSelling();
            result+= java.lang.Math.pow(rateSelling - avg, 2);
        }
        
        double resultDouble= java.lang.Math.sqrt((double)result / (double)(positions.size() - 1));
        
        return (float)resultDouble;
    }
}

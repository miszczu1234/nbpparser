/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.exception;

/**
 *
 * @author kurekk
 */
public class InputDataNotValid extends Exception{

    public InputDataNotValid() {
        super();
    }
    
    public InputDataNotValid(String message){
        super(message);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import pl.parser.nbp.data.model.ExchangeArray;
import pl.parser.nbp.data.reader.MultiplyUrlDataLoader;
import pl.parser.nbp.data.reader.UrlDataLoader;
import pl.parser.nbp.data.reader.XmlReader;
import pl.parser.nbp.data.url.RateUrlProvider;
import pl.parser.nbp.data.url.provider.algorithm.UrlProviderAlgorithm;
import pl.parser.nbp.data.url.provider.algorithm.basic.BasicUrlProviderAlgorithm;
import pl.parser.nbp.data.url.provider.algorithm.basic.UrlValidator;
import pl.parser.nbp.program.ProgramAlgorithm;
import pl.parser.nbp.program.property.PropertyHandler;
import pl.parser.nbp.program.property.PropertyList;

/**
 *
 * @author kurekk
 */
public class Test {
    
    private static final Logger LOG = Logger.getLogger(Test.class);
    
    public static void testUrlDataLoader() throws MalformedURLException{
        
        final String URL = "http://www.nbp.pl/kursy/xml/c001z020102.xml";
        
        UrlDataLoader urlDataLoader= new UrlDataLoader(URL);
        ExchangeArray exchangeArray= urlDataLoader.loadFromUrl();
        urlDataLoader.closeHttpConnection();
        if(exchangeArray== null){
            LOG.debug("Cannot open url");
            return;
        }
        
        XmlReader xmlReader= new XmlReader();
        String xmlString= xmlReader.getXmlAsString(exchangeArray);
        System.out.println(xmlString);
    }
    
    public static void testMultiplyUrlDataLoaders(){
        final String URL[] = new String[]{"http://www.nbp.pl/kursy/xml/c001z020102.xml", "http://www.nbp.pl/kursy/xml/c002z020103.xml"};
        
        MultiplyUrlDataLoader urlDataLoader= new MultiplyUrlDataLoader(URL);
        List<ExchangeArray> exchangeArray= urlDataLoader.loadFromUrl();
        
        LOG.debug(String.format("Retrived %d Exchange Arrays", exchangeArray.size()));
        
        XmlReader xmlReader= new XmlReader();
        exchangeArray.forEach((exArray) -> {
            String xmlString= xmlReader.getXmlAsString(exArray);
            System.out.println(xmlString+"\n\n");
        });
    }
    
//    public static void testRateUrlBuilder(){
//        final String desiredUrl= "http://www.nbp.pl/kursy/xml/c001z020103.xml";
//        RateUrlBuilder rateUrlBuilder= new RateUrlBuilder();
//        
//        String resultUrl= rateUrlBuilder.setRateTableNumber("001")
//                .setRateYear("02")
//                .setRateMonth("01")
//                .setRateDay("03")
//                .getRateUrl();
//        
//        LOG.debug(String.format("%s\n%s\n%s", desiredUrl.equals(resultUrl), desiredUrl, resultUrl));
//    }
    
    public static void testRateUrlProvider()throws ParseException{
        final String inputDate= "2002-01-30";
        final String url= "http://www.nbp.pl/kursy/xml/c001z020103.xml";
        
        RateUrlProvider rateUrlProvider= new RateUrlProvider();
        String outputUrl= rateUrlProvider.getUrl(inputDate);
        LOG.debug(String.format("%s\n%s\n%s", url.equals(outputUrl), url, outputUrl));
    }
    
    public static void testUrlValidator(){
        final String exsistingUrl= "http://www.nbp.pl/kursy/xml/c001z020102.xml";
        UrlValidator urlValidator= new UrlValidator();
        boolean validationResult= urlValidator.validate(exsistingUrl);
        System.out.println(validationResult);
    }
    
    public static void testBasicProviderAlgorithm() throws ParseException{
        UrlProviderAlgorithm urlProviderAlgorithm= new BasicUrlProviderAlgorithm();
        List<String> urls= urlProviderAlgorithm.getUrl("2013-01-28", "2013-01-31");
        System.out.println(Arrays.toString(urls.toArray()));
    }
    
    public static void testProgramAlgorithm() throws ParseException{
        final String beginDate= "2013-01-28";
        final String endDate= "2013-01-31";
        final String currencySymbol= "EUR";
        
        ProgramAlgorithm programAlg= new ProgramAlgorithm(beginDate, endDate, currencySymbol);
        programAlg.process();
        LOG.debug(String.format("Sredni kurs kupna : %f", programAlg.getAvgRateSelling()));
        LOG.debug(String.format("Odchylenie standardowe sprzedazy : %f", programAlg.getStandardDeviation()));
    }
    
    public static void testPropertyHandler() throws IOException{
        PropertyHandler propHandler= new PropertyHandler();
        String argsCount= propHandler.getProperty(PropertyList.ARGS_COUNT);
        LOG.debug(String.format("%s=%s", PropertyList.ARGS_COUNT.getPropertyName(), argsCount));
        Integer intArgCount= propHandler.getPropertyAsInteger(PropertyList.ARGS_COUNT);
        LOG.debug(String.format("Int argsCount : %d", intArgCount));
    }
    
    public static void main(String args[]) throws MalformedURLException, ParseException, IOException{
        //testUrlDataLoader();
        //testMultiplyUrlDataLoaders();
        //testRateUrlBuilder();
        //testRateUrlProvider();
        //testUrlValidator();
        //testBasicProviderAlgorithm();
        //testProgramAlgorithm();
        testPropertyHandler();
    }
}

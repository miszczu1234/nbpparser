/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.program.property;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author kurekk
 */
public class PropertyHandler {
    
    private static final String PROP_FILE_PATH = "app.properties";

    private Properties properties;
    
    public PropertyHandler() throws FileNotFoundException, IOException {
        InputStream inputStream= ClassLoader.getSystemResourceAsStream(PROP_FILE_PATH);
        properties= new Properties();
        properties.load(inputStream);
    }
    
    public String getProperty(PropertyList propertyName){
        return (String)properties.get(propertyName.getPropertyName());
    }
    
    public Integer getPropertyAsInteger(PropertyList propertyName){
        String propVal= getProperty(propertyName);
        if(propVal==null){
            return null;
        }
        
        return Integer.parseInt(propVal);
    }
}

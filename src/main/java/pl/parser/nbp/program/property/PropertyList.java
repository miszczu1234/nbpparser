/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.program.property;

/**
 *
 * @author kurekk
 */
public enum PropertyList {
    DATE_FORMAT("dateFormat"),
    ARGS_COUNT("argsCount"),
    CURRENCY_CODE_ARG_NUM("currencyCode"),
    VALID_CURRENCY_CODE("validCurrencyCode"),
    BEGIN_DATE("beginDate"),
    END_DATE("endDate");
    
    private String propertyName;

    private PropertyList(String propertyName) {
        this.propertyName=propertyName;
    }
    
    public String getPropertyName() {
        return propertyName;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.program.validator;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import pl.parser.nbp.program.property.PropertyHandler;
import pl.parser.nbp.program.property.PropertyList;

/**
 *
 * @author kurekk
 */
public class InputDataValidator {
    private PropertyHandler propHandler;

    private static final String ERR_ARGS_COUNT = "Niepoprawna liczba argumentów wejściowych.";
    private static final String ERR_DATE_FROMAT= "Niepoprawny format daty";
    private static final String ERR_CURR_CODE = "Nieznana waluta.";
    private static final String CURRENCY_DELIM = ",";
    
    public InputDataValidator() throws IOException {
        propHandler = new PropertyHandler();
    }
    
    private String validateArgsCount(int argsCount, String errMsg){
        int validArgsCount= propHandler.getPropertyAsInteger(PropertyList.ARGS_COUNT);
        if(argsCount == validArgsCount){
            return "";
        }
        
        return errMsg;
    }
    
    public String validateDate(String date, String errMsg){
        String datePattern= propHandler.getProperty(PropertyList.DATE_FORMAT);
        DateFormat dateFormat= new SimpleDateFormat(datePattern);
        try{
            dateFormat.parse(date);
        }catch(ParseException e){
            e.printStackTrace();
            return errMsg;
        }
        
        return "";
    }
    
    public String validateCurrencyCode(String currencyCode, String errMsg){
        String legalCurrencyCodeStr= propHandler.getProperty(PropertyList.VALID_CURRENCY_CODE);
        List<String> legalCurrencyCodeList= Arrays.asList(legalCurrencyCodeStr.split(CURRENCY_DELIM));
        if(!legalCurrencyCodeList.contains(currencyCode)){
            return errMsg;
        }
        
        return "";
    }
    
    public String validate(String args[]){
        String validateMsg= validateArgsCount(args.length, ERR_ARGS_COUNT);
        
        if(!validateMsg.isEmpty()){
            return validateMsg;
        }
        
        String beginDate= args[propHandler.getPropertyAsInteger(PropertyList.BEGIN_DATE)];
        String errMsg= ERR_DATE_FROMAT+" początkowej. "
                + "Oczekiwany format to "+propHandler.getProperty(PropertyList.DATE_FORMAT);
        validateMsg= validateDate(beginDate, errMsg);
        
        String endDate= args[propHandler.getPropertyAsInteger(PropertyList.END_DATE)];
        errMsg= ERR_DATE_FROMAT+" końcowej. "
                +"Oczekiwany format to "+propHandler.getProperty(PropertyList.DATE_FORMAT);
        
        if(validateMsg.length() > 0){
            validateMsg+="\n";
        }
        validateMsg+=validateDate(endDate, errMsg);
        
        String currencyCode= args[propHandler.getPropertyAsInteger(PropertyList.CURRENCY_CODE_ARG_NUM)];
        String legalCurrCode= propHandler.getProperty(PropertyList.VALID_CURRENCY_CODE);
        errMsg= ERR_CURR_CODE + " Dozwolone waluty to : "+legalCurrCode;
        
        if(validateMsg.length() > 0){
            validateMsg+="\n";
        }
        validateMsg+=validateCurrencyCode(currencyCode, errMsg);
        
        return validateMsg;
    }
}

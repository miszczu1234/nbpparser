/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.program;

import java.io.IOException;
import pl.parser.nbp.exception.InputDataNotValid;
import pl.parser.nbp.program.property.PropertyHandler;
import pl.parser.nbp.program.property.PropertyList;
import pl.parser.nbp.program.validator.InputDataValidator;

/**
 *
 * @author kurekk
 */
public class InputDataParser {
    
    private String[] args;
    
    private String beginDate;
    private String endDate;
    private String currencyCode;
    
    private PropertyHandler properties;
    private InputDataValidator dataValidator;
    
    private InputDataParser() throws IOException{
        properties= new PropertyHandler();
        dataValidator= new InputDataValidator();
    }
    
    public InputDataParser(String args[]) throws IOException {
        this();
        this.args= args;
    }
    
    public void parseInputData() throws InputDataNotValid{
       String validMsg= dataValidator.validate(args);
       if(!validMsg.isEmpty()){
           throw new InputDataNotValid(validMsg);
       }
       beginDate= args[properties.getPropertyAsInteger(PropertyList.BEGIN_DATE)];
       endDate= args[properties.getPropertyAsInteger(PropertyList.END_DATE)];
       currencyCode= args[properties.getPropertyAsInteger(PropertyList.CURRENCY_CODE_ARG_NUM)];
    }

    public String getBeginDate() {
        return beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.program;

import java.text.ParseException;
import java.util.List;
import org.apache.log4j.Logger;
import pl.parser.nbp.data.DataHandler;
import pl.parser.nbp.data.model.Position;
import pl.parser.nbp.math.Math;

/**
 *
 * @author kurekk
 */
public class ProgramAlgorithm {
    
    private static final Logger LOG= Logger.getLogger(ProgramAlgorithm.class);
    
    private String beginDate;
    private String endDate;
    private String currencyCode;
    
    private DataHandler dataHandler;
    private Math math;
    
    private float avgRateSelling;
    private float standardDeviation;
    
    public ProgramAlgorithm() {
        this.dataHandler= new DataHandler();
        math= new Math();
        avgRateSelling= 0;
        standardDeviation= 0;
    }

    public ProgramAlgorithm(String beginDate, String endDate, String currencySymbol){
        this();
        this.beginDate= beginDate;
        this.endDate= endDate;
        this.currencyCode= currencySymbol;
    }

    public float getAvgRateSelling() {
        return avgRateSelling;
    }

    public float getStandardDeviation() {
        return standardDeviation;
    }
    
    public void process() throws ParseException{
        dataHandler.loadData(beginDate, endDate);
        LOG.info("Przetwarzanie danych...");
        List<Position> positions= dataHandler.getByCurrencyCode(currencyCode);
        avgRateSelling= math.calculateAvgRateBuying(positions);
        standardDeviation= math.calculateStandardDeviation(positions);
    }
}

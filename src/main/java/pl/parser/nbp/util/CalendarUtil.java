/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author kurekk
 */
public class CalendarUtil {

    public String getAsString(Calendar cal, int type) {
        Integer time = cal.get(type);

        if (type == Calendar.MONTH) {
            time += 1;
        }

        return String.format("%02d", time);
    }

    public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new LinkedList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(enddate);
        calendar.add(calendar.DATE, 1);
        Date endDateLimit= calendar.getTime();
        calendar.setTime(startdate);
        
        while (calendar.getTime().before(endDateLimit)) {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        
        return dates;
    }
}

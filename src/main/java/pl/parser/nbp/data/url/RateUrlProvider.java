/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.url;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import pl.parser.nbp.util.CalendarUtil;

/**
 *
 * @author kurekk
 */
public class RateUrlProvider {
    
    private final String DATE_PATTERN = "yyyy-MM-dd";
    private DateFormat dateFormat;
    private CalendarUtil calendarUtil;
    
    public RateUrlProvider() {
        dateFormat= new SimpleDateFormat(DATE_PATTERN);
        calendarUtil= new CalendarUtil();
    }
    
    public String getUrl(String strRateDate) throws ParseException{
        Date rateDate= dateFormat.parse(strRateDate);
        Calendar calendar= Calendar.getInstance();
        calendar.setTime(rateDate);
        
        String year= calendarUtil.getAsString(calendar, Calendar.YEAR);
        year= year.substring(year.length()-2, year.length());
        String month= calendarUtil.getAsString(calendar, Calendar.MONTH);
        String day= calendarUtil.getAsString(calendar, Calendar.DAY_OF_MONTH);
        
        String url= new RateUrlBuilder().setRateDay(day)
                .setRateMonth(month)
                .setRateYear(year)
                .getRateUrl();
        
        return url;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.url;

/**
 *
 * @author kurekk
 */
class RateUrlBuilder {
    
    private String patternUrl= "http://www.nbp.pl/kursy/xml/";
    
    private final String patternFileName= 
            "c:"+PatternFilenameSymbol.TABLE_NUM.getSymbol()+"z"+":"+
            PatternFilenameSymbol.RATE_YEAR.getSymbol()+":"+
            PatternFilenameSymbol.RATE_MONTH.getSymbol()+":"+
            PatternFilenameSymbol.RATE_DAY.getSymbol()+".xml";
    
    private String url;
    
    public RateUrlBuilder(){
        url= patternFileName;
    }
    
    public RateUrlBuilder setRateTableNumber(String nnn){
        String toReplace= ":"+PatternFilenameSymbol.TABLE_NUM.getSymbol();
        url= url.replace(toReplace, nnn);

        
        return this;
    }
    
    public RateUrlBuilder setRateYear(String yyyy){
        String toReplace= ":"+PatternFilenameSymbol.RATE_YEAR.getSymbol();
        url= url.replace(toReplace, yyyy);
    
        return this;
    }
    
    public RateUrlBuilder setRateMonth(String mm){
        String toReplace= ":"+PatternFilenameSymbol.RATE_MONTH.getSymbol();
        url= url.replace(toReplace, mm);
    
        return this;
    }
    
    public RateUrlBuilder setRateDay(String dd){
        String toReplace= ":"+PatternFilenameSymbol.RATE_DAY.getSymbol();
        url= url.replace(toReplace, dd);
        
        return this;
    }
    
    public String getRateUrl(){
        return patternUrl+url;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.url;

/**
 *
 * @author kurekk
 */
public enum PatternFilenameSymbol {
    TABLE_NUM("nnn"),
    RATE_YEAR("yyy"),
    RATE_MONTH("mm"),
    RATE_DAY("dd");
    
    private String symbol;

    private PatternFilenameSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.url.provider.algorithm.basic;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.log4j.Logger;

/**
 *
 * @author kurekk
 */
public class UrlValidator {
    
    private static final Logger LOG= Logger.getLogger(UrlValidator.class);
    
    public boolean validate(String url){
        
        try{
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection conn= (HttpURLConnection)new URL(url).openConnection();
            conn.setRequestMethod("HEAD");
            Integer responseCode= conn.getResponseCode();
            LOG.debug(String.format("validate() : url %s, response code %d", url, responseCode));
            if(responseCode== HttpURLConnection.HTTP_OK){
                return true;
            }
        }catch(Exception e){
            LOG.debug(String.format("validate() : exception %s", e));
            return false;
        }
        
        return false;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.url.provider.algorithm;

import java.text.ParseException;
import java.util.List;

/**
 *
 * @author kurekk
 */
public interface UrlProviderAlgorithm {
    List<String> getUrl(String beginDate, String endDate) throws ParseException;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.url.provider.algorithm.basic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import pl.parser.nbp.data.url.PatternFilenameSymbol;
import pl.parser.nbp.data.url.RateUrlProvider;
import pl.parser.nbp.data.url.provider.algorithm.UrlProviderAlgorithm;
import pl.parser.nbp.util.CalendarUtil;

/**
 *
 * @author kurekk
 */
public class BasicUrlProviderAlgorithm implements UrlProviderAlgorithm{

    private static final Logger LOG= Logger.getLogger(BasicUrlProviderAlgorithm.class);
    
    private final String DATE_PATTERN= "yyy-MM-dd";
    private DateFormat dateFormat;
    private CalendarUtil calendarUtil;
    private RateUrlProvider rateUrlProvider;
    private UrlValidator urlValidator;
    
    private static final Integer rateNumberBegin= 0; 
    private static final Integer rateNumberLimit = 999;
    
    class TableRateEntry implements Map.Entry<Date, Integer>{

        private Date key;
        private Integer value;

        public TableRateEntry(Date key, Integer value) {
            this.key = key;
            this.value = value;
        }
        
        public Date setKey(Date key){
            Date oldKey= this.key;
            this.key= key;
            
            return oldKey;
        }
        
        @Override
        public Date getKey() {
            return key;
        }

        @Override
        public Integer getValue() {
            return value;
        }

        @Override
        public Integer setValue(Integer value) {
            Integer oldValue= this.value;
            this.value= value;
            
            return oldValue;
        }
        
    }
    
    public BasicUrlProviderAlgorithm() {
        dateFormat= new SimpleDateFormat(DATE_PATTERN);
        calendarUtil= new CalendarUtil();
        rateUrlProvider= new RateUrlProvider();
        urlValidator= new UrlValidator();
    }
    
    public String getUrl(Date date, int tableRateNum) throws ParseException{
        String url= rateUrlProvider.getUrl(dateFormat.format(date));
        String tableNum= String.format("%03d", tableRateNum);
        String toReplace = ":"+PatternFilenameSymbol.TABLE_NUM.getSymbol();
        String urlToCheck= url.replace(toReplace, tableNum);
        
        return urlToCheck;
    }
    
    private int findTableRateNum(Date date) throws ParseException{
        
        for(int i= rateNumberBegin; i<= rateNumberLimit; i++){
            String urlToCheck= getUrl(date, i); 
            if(urlValidator.validate(urlToCheck)){
                LOG.debug(String.format("findRate() : found url rate: %s", urlToCheck));
                return i;
            }
            //LOG.debug(String.format("findRate() : url %s doesn't contain rate", urlToCheck));
        }
        
        return -1;
    }
    
    private Entry<Date, Integer> findFirstTableRateNum(List<Date> dates) throws ParseException{
        
        for(Date date : dates){
            int rateNum= -1;
            if((rateNum= findTableRateNum(date)) > 0){
                TableRateEntry rateEntry= new TableRateEntry(date, rateNum);
                return rateEntry;
            }
        }
        
        return null;
    }
    
    private String retriveUrl(Date date, Integer rateTableNum) throws ParseException{
        String url= getUrl(date, rateTableNum);
        if(urlValidator.validate(url)){
            return url;
        }
        
        return "";
    }
    
    private List<String> retriveUrls(List<Date> dates) throws ParseException{
        List<String> urls= new LinkedList<>();
        
        Entry<Date, Integer> firstRate= findFirstTableRateNum(dates);
        if(firstRate == null){
            return urls;
        }
        String firstUrl= getUrl(firstRate.getKey(), firstRate.getValue());
        urls.add(firstUrl);
        int rateNum= firstRate.getValue() + 1;
        
        for(Date date: dates){
            if(date.compareTo(firstRate.getKey()) <= 0){
                continue;
            }
            String url= retriveUrl(date, rateNum);
            if(!url.isEmpty()){
                urls.add(url);
                rateNum+=1;
            }
        }
        
        return urls;
    }
    
    @Override
    public List<String> getUrl(String strBeginDate, String strEndDate)throws ParseException{
        Date beginDate= dateFormat.parse(strBeginDate);
        Date endDate= dateFormat.parse(strEndDate);
     
        List<Date> datesBetween= calendarUtil.getDaysBetweenDates(beginDate, endDate);
        List<String> urls= retriveUrls(datesBetween);
        
        return urls;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import pl.parser.nbp.data.model.ExchangeArray;
import pl.parser.nbp.data.model.Position;
import pl.parser.nbp.data.reader.MultiplyUrlDataLoader;
import pl.parser.nbp.data.url.provider.algorithm.UrlProviderAlgorithm;
import pl.parser.nbp.data.url.provider.algorithm.basic.BasicUrlProviderAlgorithm;

/**
 *
 * @author kurekk
 */
public class DataHandler {
    
    private static final Logger LOG= Logger.getLogger(DataHandler.class);
    
    private UrlProviderAlgorithm urlProvider;
    private MultiplyUrlDataLoader urlDataLoader;
    
    private List<ExchangeArray> data;
    
    public DataHandler() {
        urlProvider= new BasicUrlProviderAlgorithm();
        data= new LinkedList<>();
    }
    
    public List<Position> getByCurrencyCode(String currencyCode){
        
        List<Position> result= new ArrayList<>();
        
        for(ExchangeArray exchangeArray : data){
            for(Position position : exchangeArray.getPositions()){
                if(position.getCurrencyCode().equals(currencyCode)){
                    result.add(position);
                }
            }
        }
        
        return result;
    }
    
    public List<ExchangeArray> loadData(String beginDate, String endDate) throws ParseException{
        LOG.info("Pobieranie informacji o adresach URL...");
        List<String> urls= urlProvider.getUrl(beginDate, endDate);
        LOG.info(String.format("Znaleziono %d adrsów URL z danymi.\nWczytywanie danych...", urls.size()));
        urlDataLoader= new MultiplyUrlDataLoader(urls.toArray(new String[urls.size()]));
        data= urlDataLoader.loadFromUrl();
        
        return data;
    }
}

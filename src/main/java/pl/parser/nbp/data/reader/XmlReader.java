/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

/**
 *
 * @author kurekk
 */
public class XmlReader {

    private static final Logger LOG = Logger.getLogger(XmlReader.class);

    public <T> String getXmlAsString(T inputDataXmlModel) {

        StringWriter stringWriter = new StringWriter();
        String marshalledInput = "";

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(inputDataXmlModel.getClass());
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(inputDataXmlModel, stringWriter);
            marshalledInput = stringWriter.toString();
        } catch (JAXBException e) {
            LOG.info(String.format("getXmlAsString() : Cannot display xml, JAXBException : %s", e));
        } finally {
            try {
                stringWriter.close();
            } catch (IOException e) {
                LOG.error(String.format("getXmlAsString() : Cannot close StringWriter, exception %s", e));
            }
        }

        return marshalledInput;
    }

    private SAXSource getSAXSource(InputStream inputStream) throws ParserConfigurationException, SAXException {
        SAXParserFactory saxParseFactory = SAXParserFactory.newInstance();
        saxParseFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        XMLReader xmlReader = saxParseFactory.newSAXParser().getXMLReader();
        InputSource inputSource = new InputSource(inputStream);
        SAXSource saxSource = new SAXSource(xmlReader, inputSource);
        
        return saxSource;
    }

    public <T> T read(InputStream inputStream, Class<T> typeClass) throws JAXBException {

        T loaded = null;

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(typeClass);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            SAXSource saxSource= getSAXSource(inputStream);
            loaded = (T) jaxbUnmarshaller.unmarshal(saxSource);
        } catch (ParserConfigurationException e) {
            LOG.debug(String.format("read() : Parser Configuration Exception %s", e));
        } catch (SAXException e) {
            LOG.debug(String.format("read() : SAX Exception %s", e));
        } catch (JAXBException e) {
            LOG.debug(String.format("read() : JAXBException %s", e));
        }

        return loaded;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.reader;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author kurekk
 */
public class UrlReader {
    
    private URL url;
    private HttpURLConnection httpURLConn;
    
    public UrlReader(String url) throws MalformedURLException{
        this.url= new URL(url);
    }
    
    private HttpURLConnection getConnection() throws ProtocolException, IOException{
        HttpURLConnection conn= (HttpURLConnection)url.openConnection();
        
        return conn;
    }
    
    public void closeHttpURLConnection(){
        httpURLConn.disconnect();
    }

    public HttpURLConnection getHttpURLConn() {
        return httpURLConn;
    }
    
    public InputStream getInputStream() throws IOException{
        httpURLConn= getConnection();
        
        return httpURLConn.getInputStream();
    }
    
    public URL getUrl(){
        return url;
    }
}

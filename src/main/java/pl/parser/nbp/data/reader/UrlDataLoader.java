package pl.parser.nbp.data.reader;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.bind.JAXBException;
import org.apache.log4j.Logger;
import pl.parser.nbp.data.model.ExchangeArray;

/**
 *
 * @author kurekk
 */
public class UrlDataLoader {
    
    private static final Logger LOG= Logger.getLogger(UrlDataLoader.class);
    
    private UrlReader urlReader;
    private XmlReader xmlReader;
  
    public UrlDataLoader(String url) throws MalformedURLException{
        this.urlReader= new UrlReader(url);
        this.xmlReader= new XmlReader();
    }
    
    public void closeHttpConnection(){
        urlReader.closeHttpURLConnection();
    }
    
    public ExchangeArray loadFromUrl(){
        
        InputStream inputStream= null;
        
        try{
            inputStream= urlReader.getInputStream();
            ExchangeArray exchangeArray= xmlReader.read(inputStream, ExchangeArray.class);
            URL url= urlReader.getUrl();
            LOG.info(String.format("Pobrano dane z adresu : %s", url.toString()));
            return exchangeArray;
        }catch(IOException e){
            LOG.debug(String.format("loadFromUrl() : get input strem from url exception %s", e));
        } catch (JAXBException ex) {
            LOG.debug(String.format("loadFromUrl() : JAXBException %s", ex));
        }finally{
            if(inputStream!=null){
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    LOG.debug(String.format("loadFromUrl() : close stream exception %s", ex));
                }
            }
        }
        
        return null;
    }
    
    public String getURL(){
        return urlReader.getUrl().toString();
    }
}

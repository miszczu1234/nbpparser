/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.reader;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import pl.parser.nbp.data.model.ExchangeArray;

/**
 *
 * @author kurekk
 */
public class MultiplyUrlDataLoader {
    
    private List<UrlDataLoader> urlDataLoaders;

    private static final Logger LOG= Logger.getLogger(MultiplyUrlDataLoader.class);
    
    public MultiplyUrlDataLoader() {
        urlDataLoaders= new LinkedList<>();
    }
    
    private List<UrlDataLoader> getLoaders(String... urls){
        List<UrlDataLoader> urlDataLoaders= new LinkedList<>();
        
        for(String url : urls){
            try{
                UrlDataLoader urlDataLoader= new UrlDataLoader(url);
                urlDataLoaders.add(urlDataLoader);
            }catch(MalformedURLException e){
                LOG.debug(String.format("Cannot get data from URL : %s", url));
            }
            
        }
        
        return urlDataLoaders;
    }
    
    public MultiplyUrlDataLoader(String... urls){
        this();
        urlDataLoaders.addAll(getLoaders(urls));
        LOG.debug(String.format("MultiplyUrlDataLoader() : Retrived %d files with data", urlDataLoaders.size()));
    }
    
    public List<ExchangeArray> loadFromUrl(){
        List<ExchangeArray> exchangeArrays= new LinkedList<>();
        
        urlDataLoaders.forEach((urlDataLoader) -> {
            ExchangeArray exchangeArray= urlDataLoader.loadFromUrl();
            if(exchangeArray == null){
                LOG.info(String.format("loadFromUrl() : Cannot read Exchange Array from url %s", urlDataLoader.getURL()));
            }else{
                LOG.debug("loadFromUrl() : Retrived Exchange Array");
                exchangeArrays.add(exchangeArray);
            }
            urlDataLoader.closeHttpConnection();
        });
        
        return exchangeArrays;
    }
}

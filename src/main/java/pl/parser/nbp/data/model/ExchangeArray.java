/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.model;

import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import pl.parser.nbp.data.model.adapter.DateModelAdapter;

/**
 *
 * @author kurekk
 */

@XmlRootElement(name="tabela_kursow")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeArray {
    
    @XmlAttribute(name = "typ")
    private String type;
    
    @XmlElement(name= "numer_tabeli")
    private String tableNumber;
    
    @XmlElement(name= "data_notowania")
    @XmlJavaTypeAdapter(DateModelAdapter.class)
    private Date listingDate;
    
    @XmlElement(name= "data_publikacji")
    @XmlJavaTypeAdapter(DateModelAdapter.class)
    private Date publicationDate;
    
    @XmlElement(name= "pozycja")
    private List<Position> positions;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }
    
    public String getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }

    public Date getListingDate() {
        return listingDate;
    }

    public void setListingDate(Date listingDate) {
        this.listingDate = listingDate;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public String toString() {
        return "ExchangeArray{" + "type=" + type + ", tableNumber=" + tableNumber + ", listingDate=" + listingDate + ", publicationDate=" + publicationDate + ", positions=" + positions + '}';
    }
}

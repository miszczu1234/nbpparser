/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.model.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author kurekk
 */
public class RateModelAdapter extends XmlAdapter<String, Float>{

    private static final String OLD_DELIM = ",";
    private static final String NEW_DELIM = "\\.";
    
    @Override
    public Float unmarshal(String str) throws Exception {
        String rate= str.replaceAll(OLD_DELIM, NEW_DELIM);
        return Float.parseFloat(rate);
    }

    @Override
    public String marshal(Float v) throws Exception {
        return v.toString();
    }
    
}

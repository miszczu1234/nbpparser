/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.model.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.xml.sax.helpers.XMLReaderAdapter;

/**
 *
 * @author kurekk
 */
public class DateModelAdapter extends XmlAdapter<String, Date>{

    private final SimpleDateFormat dateFormat= new SimpleDateFormat("yyy-MM-dd");
    
    @Override
    public Date unmarshal(String str) throws Exception {
       return dateFormat.parse(str);
    }

    @Override
    public String marshal(Date date) throws Exception {
        return dateFormat.format(date);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.data.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import pl.parser.nbp.data.model.adapter.DateModelAdapter;
import pl.parser.nbp.data.model.adapter.RateModelAdapter;

/**
 *
 * @author kurekk
 */

@XmlRootElement(name = "pozycja")
@XmlAccessorType(XmlAccessType.FIELD)
public class Position {
    
    @XmlElement(name = "nazwa_kraju")
    private String country;
    
    @XmlElement(name = "symbol_waluty")
    private Integer currencySymbol;
    
    @XmlElement(name = "przelicznik")
    private Integer converter;
    
    @XmlElement(name = "kod_waluty")
    private String currencyCode;
    
    @XmlElement(name = "kurs_kupna")
    @XmlJavaTypeAdapter(RateModelAdapter.class)
    private Float rateBuying;
    
    @XmlElement(name= "kurs_sprzedazy")
    @XmlJavaTypeAdapter(RateModelAdapter.class)
    private Float rateSelling;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(Integer currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public Integer getConverter() {
        return converter;
    }

    public void setConverter(Integer converter) {
        this.converter = converter;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Float getRateBuying() {
        return rateBuying;
    }

    public void setRateBuying(Float rateBuying) {
        this.rateBuying = rateBuying;
    }

    public Float getRateSelling() {
        return rateSelling;
    }

    public void setRateSelling(Float rateSelling) {
        this.rateSelling = rateSelling;
    }

    @Override
    public String toString() {
        return "Position{" + "country=" + country + ", currencySymbol=" + currencySymbol + ", converter=" + converter + ", currencyCode=" + currencyCode + ", rateBuying=" + rateBuying + ", rateSelling=" + rateSelling + '}';
    }
}
